package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToGetRegisteredCarsException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToGetRegisteredCarsException() {
    }

    public UnableToGetRegisteredCarsException(String message) {
        super(message);
    }

    public UnableToGetRegisteredCarsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToGetRegisteredCarsException(Throwable cause) {
        super(cause);
    }
}
