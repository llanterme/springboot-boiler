package za.co.clique.core.domain;

/**
 * Created by Luke on 10/25/17.
 */
public class Push {

    private int badge;
    private String sound;
    private String body;
    private String deviceToken;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public int getBadge() {
        return badge;
    }

    public void setBadge(int badge) {
        this.badge = badge;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public Push(int badge, String sound, String body) {
        this.badge = badge;
        this.sound = sound;
        this.body = body;
    }

    public Push() {
    }
}
