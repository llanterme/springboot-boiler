package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToGetUserGraphException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToGetUserGraphException() {
    }

    public UnableToGetUserGraphException(String message) {
        super(message);
    }

    public UnableToGetUserGraphException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToGetUserGraphException(Throwable cause) {
        super(cause);
    }
}
