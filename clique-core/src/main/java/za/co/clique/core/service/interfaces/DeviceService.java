package za.co.clique.core.service.interfaces;

import za.co.clique.core.domain.Device;
import za.co.clique.core.exceptions.ServiceException;

import javax.xml.ws.Service;

public interface DeviceService {


    Device registerDevice(Device device) throws ServiceException;

    Device getUserDevice(int Id) throws ServiceException;


}
