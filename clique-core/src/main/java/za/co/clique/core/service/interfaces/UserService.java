package za.co.clique.core.service.interfaces;


import za.co.clique.core.domain.Api;
import za.co.clique.core.domain.User;
import za.co.clique.core.exceptions.ServiceException;

public interface UserService {

    User registerUser(User user) throws ServiceException;

    User getUserGraph(int userId) throws ServiceException;

    User authUser(String emailAddress, String password) throws ServiceException;
}
