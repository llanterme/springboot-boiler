package za.co.clique.core.exceptions;

public class ServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
