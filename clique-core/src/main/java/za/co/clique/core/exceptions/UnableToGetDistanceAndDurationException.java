package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToGetDistanceAndDurationException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToGetDistanceAndDurationException() {
    }

    public UnableToGetDistanceAndDurationException(String message) {
        super(message);
    }

    public UnableToGetDistanceAndDurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToGetDistanceAndDurationException(Throwable cause) {
        super(cause);
    }
}
