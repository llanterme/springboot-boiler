package za.co.clique.core.service.interfaces;

import za.co.clique.core.domain.Request;
import za.co.clique.core.exceptions.ServiceException;

import java.util.List;

public interface RequestService {

    Request createRequest(Request request) throws ServiceException;

    Request getRequest(int requestId) throws ServiceException;

    Request getActiveUserRequest(int userId) throws ServiceException;

    List<Request> getActiveRequests(String technicianLocation) throws ServiceException;

    Request acceptRequest(int requestId, int responseId) throws ServiceException;


}
