package za.co.clique.core.service.interfaces;

import za.co.clique.core.domain.Push;
import za.co.clique.core.exceptions.ServiceException;

public interface PushMessasgeService {

    Push testPush(Push pushDetails, String keyType) throws ServiceException;
}
