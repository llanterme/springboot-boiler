package za.co.clique.core.domain;

import java.sql.Timestamp;


public class Request {

    private int requestId;
    private int userId;
    private int categoryId;
    private int carId;
    private String coords;
    private String status;
    private String requestTime;
    private String notes;
    private String address;
    private String UserToTechnicianDistance;
    private String UserToTechnicianDuration;
    private  Car car;
    private Category category;

    public String getUserToTechnicianDistance() {
        return UserToTechnicianDistance;
    }

    public void setUserToTechnicianDistance(String userToTechnicianDistance) {
        UserToTechnicianDistance = userToTechnicianDistance;
    }

    public String getUserToTechnicianDuration() {
        return UserToTechnicianDuration;
    }

    public void setUserToTechnicianDuration(String userToTechnicianDuration) {
        UserToTechnicianDuration = userToTechnicianDuration;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCoords() {
        return coords;
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }
}
