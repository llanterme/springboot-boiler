package za.co.clique.core.service.interfaces;


import za.co.clique.core.domain.Api;
import za.co.clique.core.domain.Technician;
import za.co.clique.core.exceptions.ServiceException;

public interface TechnicianService {

    Technician getTechnician(int technicianId) throws ServiceException;

    Technician authenticationTechnician(String emailAddress, String password) throws  ServiceException;
}
