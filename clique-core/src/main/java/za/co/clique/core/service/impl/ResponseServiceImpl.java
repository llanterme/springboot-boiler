package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.*;
import za.co.clique.core.exceptions.ServiceException;
import za.co.clique.core.exceptions.TechnicianAlreadyRespondedException;
import za.co.clique.core.exceptions.UnableToCreateRequestException;
import za.co.clique.core.service.interfaces.*;
import za.co.clique.core.utils.Utils;

import za.co.clique.infrastructure.dao.db.entities.RequestEntity;
import za.co.clique.infrastructure.dao.db.entities.ResponseEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;
import za.co.clique.infrastructure.dao.db.repo.RequestRepository;
import za.co.clique.infrastructure.dao.db.repo.ResponseRepository;
import za.co.clique.infrastructure.dao.rest.entities.DistanceMatrixEntity;
import za.co.clique.infrastructure.dao.rest.interfaces.DistanceRestDao;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;


@Service
public class ResponseServiceImpl implements ResponseService {


    @Autowired
    ResponseRepository responseRepository;

    @Autowired
    RequestService requestService;

    @Autowired
    TechnicianService technicianService;

    @Autowired
    DeviceService deviceService;

    @Autowired
    PushMessasgeService pushMessasgeService;

    @Autowired
    UserService userService;

    @Autowired
    ResponseService responseService;

    @Override
    public Response createResponse(Response technicianResponse) throws ServiceException {
        try {


            if(responseRepository.technicianAlreadyResponded(technicianResponse.getTechnicianId(),technicianResponse.getRequestId())) {
                throw new TechnicianAlreadyRespondedException();
            }
            ResponseEntity newResponse = new ResponseEntity();
            newResponse.setStatus(technicianResponse.getStatus());
            newResponse.setTechnicianId(technicianResponse.getTechnicianId());
            newResponse.setRequestId(technicianResponse.getRequestId());
            newResponse.setLastLocation(technicianResponse.getLastLocation());
            newResponse.setResponseTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));

            technicianResponse.setResponseId(responseRepository.save(newResponse).getResponseId());


            generatePushForUser(technicianResponse.getRequestId(), "user");

            return technicianResponse;


        } catch (DataAccessException e) {
            throw new ServiceException("Unable to register technician response.");
        }
    }

    @Override
    public List<Response> getTechnicianResponses(int requestId) throws ServiceException {

        try {

            List<Response> techniciansResponses = new ArrayList<>();

            // getRequest details
            Request request = requestService.getRequest(requestId);

            for(ResponseEntity dbResponse: responseRepository.getTechnicianResponses(requestId)) {

                // getTechnicianDetails
                Technician technician = technicianService.getTechnician(dbResponse.getTechnicianId());

                //get DistanceMatrix
                DistanceMatrixEntity distanceMatrixEntity = Utils.getDistanceMatrix(request.getCoords(), dbResponse.getLastLocation());


                Response aResponse = new Response();
                aResponse.setResponseId(dbResponse.getResponseId());
                aResponse.setRequestId(dbResponse.getRequestId());
                aResponse.setTechnicianId(dbResponse.getTechnicianId());
                aResponse.setTechnicianToUserDuration(distanceMatrixEntity.getDuration());
                aResponse.setTechnicianToUserDistance(distanceMatrixEntity.getDistance());

                aResponse.setTechnicianContact(technician.getTelephone());
                aResponse.setTechnicianRate(technician.getHourRate());
                aResponse.setTechnicianName(technician.getName());
                aResponse.setStatus(dbResponse.getStatus());


                techniciansResponses.add(aResponse);
            }

            return techniciansResponses;
        } catch (DataAccessException e) {
            throw new ServiceException("Unable to build technician responses");
        }

    }

    @Override
    public Response selectTechnicianForJob(int responseId) throws ServiceException {
        try {

           ResponseEntity responseEntity = responseRepository.findOne(responseId);
            responseEntity.setStatus("Selected");
            responseRepository.save(responseEntity);

            Response response = getResponse(responseId);
            response.setStatus(responseEntity.getStatus());

            generatePushForTechnician(responseId, "technician");

            return response;



        } catch (DataAccessException e) {
            throw new ServiceException("Unable to update technician status for response.");
        }
    }

    @Override
    public Response getResponse(int responseId) throws ServiceException {

        try {
                ResponseEntity dbResponse = responseRepository.findOne(responseId);

            // getRequest details
            Request request = requestService.getRequest(dbResponse.getRequestId());

            // getTechnicianDetails
            Technician technician = technicianService.getTechnician(dbResponse.getTechnicianId());

            //get DistanceMatrix
            DistanceMatrixEntity distanceMatrixEntity = Utils.getDistanceMatrix(request.getCoords(), dbResponse.getLastLocation());

            Response response = new Response();
            response.setResponseId(dbResponse.getResponseId());
            response.setRequestId(dbResponse.getRequestId());
            response.setTechnicianId(dbResponse.getTechnicianId());
            response.setTechnicianToUserDuration(distanceMatrixEntity.getDuration());
            response.setTechnicianToUserDistance(distanceMatrixEntity.getDistance());
            response.setTechnicianContact(technician.getTelephone());
            response.setTechnicianRate(technician.getHourRate());
            response.setTechnicianName(technician.getName());
            response.setStatus(dbResponse.getStatus());

            return response;

        } catch (DataAccessException e) {
            throw new ServiceException();
        }



    }

    @Override
    public void deleteResponses(int requestId) throws ServiceException {

        try {
            for(ResponseEntity dbResponse: responseRepository.getTechnicianResponses(requestId)) {
                responseRepository.deleteResponses(dbResponse.getRequestId());
            }


        } catch (DataAccessException e) {
            throw new ServiceException("Unable to delete rejected responses");
        }

    }

    private void generatePushForUser(int requestId, String key ) throws ServiceException {
        try {

            //get the request
            Request request = requestService.getRequest(requestId);

            //get device
            Device userDevice = deviceService.getUserDevice(request.getUserId());

            //send push
            Push push = new Push();
            push.setDeviceToken(userDevice.getDeviceToken());
            push.setSound("ping.aiff");
            push.setBadge(0);
            push.setBody("A technician has responded.");

            pushMessasgeService.testPush(push, key);
        } catch (Exception e) {
            throw new ServiceException();
        }
    }

    private void generatePushForTechnician(int requestId, String key ) throws ServiceException {
        try {

            //get the response
            Response response = responseService.getResponse(requestId);

            //get device
            Device userDevice = deviceService.getUserDevice(response.getTechnicianId());

            //send push
            Push push = new Push();
            push.setDeviceToken(userDevice.getDeviceToken());
            push.setSound("ping.aiff");
            push.setBadge(0);
            push.setBody("You have been accepted. Please proceed to user.");

            pushMessasgeService.testPush(push, key);
        } catch (Exception e) {
            throw new ServiceException();
        }
    }

}
