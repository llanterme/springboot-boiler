package za.co.clique.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.clique.core.exceptions.ServiceException;
import za.co.clique.core.exceptions.UnableToGetDistanceAndDurationException;
import za.co.clique.infrastructure.dao.rest.entities.DistanceMatrixEntity;
import za.co.clique.infrastructure.dao.rest.impl.DistanceRestDaoImpl;
import za.co.clique.infrastructure.dao.rest.interfaces.DistanceRestDao;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.util.HashMap;
import java.util.regex.Pattern;

@Service
public class Utils {

    @Autowired
     static DistanceRestDao distanceRestDao;

    public static DistanceMatrixEntity getDistanceMatrix(String userLocation, String techLocation) throws ServiceException {

        distanceRestDao = new DistanceRestDaoImpl();

        DistanceMatrixEntity values;
        HashMap<String, String> coOrds = new HashMap<>();

        String[] userLocationRaw = userLocation.split(Pattern.quote("|"));
        String[] techLocationRaw = techLocation.split(Pattern.quote("|"));


        coOrds.put("userLat", userLocationRaw[0]);
        coOrds.put("userLong", userLocationRaw[1]);

        coOrds.put("techlat", techLocationRaw[0]);
        coOrds.put("techLong", techLocationRaw[1]);

        try {
            values = distanceRestDao.getTechnicianDistanceMatrix(coOrds);
        } catch (DaoException e) {
            throw new UnableToGetDistanceAndDurationException();
        }

        return new DistanceMatrixEntity(values.getDistance(),values.getDuration());

    }
}
