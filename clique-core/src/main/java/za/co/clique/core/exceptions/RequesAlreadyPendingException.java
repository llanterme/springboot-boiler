package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class RequesAlreadyPendingException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public RequesAlreadyPendingException() {
    }

    public RequesAlreadyPendingException(String message) {
        super(message);
    }

    public RequesAlreadyPendingException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequesAlreadyPendingException(Throwable cause) {
        super(cause);
    }
}
