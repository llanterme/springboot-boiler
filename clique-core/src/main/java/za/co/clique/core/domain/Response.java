package za.co.clique.core.domain;


public class Response {
    private int responseId;
    private int requestId;
    private int technicianId;
    private String technicianToUserDistance;
    private String technicianToUserDuration;
    private Double technicianRate;
    private String technicianName;
    private String technicianContact;
    private String status;
    private  String lastLocation;

    public String getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(String lastLocation) {
        this.lastLocation = lastLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getResponseId() {
        return responseId;
    }

    public void setResponseId(int responseId) {
        this.responseId = responseId;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(int technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianToUserDistance() {
        return technicianToUserDistance;
    }

    public void setTechnicianToUserDistance(String technicianToUserDistance) {
        this.technicianToUserDistance = technicianToUserDistance;
    }

    public String getTechnicianToUserDuration() {
        return technicianToUserDuration;
    }

    public void setTechnicianToUserDuration(String technicianToUserDuration) {
        this.technicianToUserDuration = technicianToUserDuration;
    }

    public Double getTechnicianRate() {
        return technicianRate;
    }

    public void setTechnicianRate(Double technicianRate) {
        this.technicianRate = technicianRate;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public String getTechnicianContact() {
        return technicianContact;
    }

    public void setTechnicianContact(String technicianContact) {
        this.technicianContact = technicianContact;
    }
}
