package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class TechnicianAlreadyRespondedException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public TechnicianAlreadyRespondedException() {
    }

    public TechnicianAlreadyRespondedException(String message) {
        super(message);
    }

    public TechnicianAlreadyRespondedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicianAlreadyRespondedException(Throwable cause) {
        super(cause);
    }
}
