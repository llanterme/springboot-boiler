package za.co.clique.core.domain;


public class TechnicianRating {

    private int technicianRatingId;
    private int userCount;
    private int ratingCount;
    private int technicianId;
    private int stars;

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getTechnicianRatingId() {
        return technicianRatingId;
    }

    public void setTechnicianRatingId(int technicianRatingId) {
        this.technicianRatingId = technicianRatingId;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public int getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(int technicianId) {
        this.technicianId = technicianId;
    }



}
