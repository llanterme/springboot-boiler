package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class TechnicianNotVerifiedException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public TechnicianNotVerifiedException() {
    }

    public TechnicianNotVerifiedException(String message) {
        super(message);
    }

    public TechnicianNotVerifiedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicianNotVerifiedException(Throwable cause) {
        super(cause);
    }
}
