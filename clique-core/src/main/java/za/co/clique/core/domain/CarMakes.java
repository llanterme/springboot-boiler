package za.co.clique.core.domain;

/**
 * Created by Luke on 6/19/17.
 */
public class CarMakes {

    private int makeId;

    private String make;

    private int carMakeWebId;

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public int getCarMakeWebId() {
        return carMakeWebId;
    }

    public void setCarMakeWebId(int carMakeWebId) {
        this.carMakeWebId = carMakeWebId;
    }
}
