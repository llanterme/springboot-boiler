package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Car;
import za.co.clique.core.domain.Device;
import za.co.clique.core.domain.Request;
import za.co.clique.core.domain.User;
import za.co.clique.core.exceptions.*;
import za.co.clique.core.service.interfaces.DeviceService;
import za.co.clique.core.service.interfaces.RequestService;
import za.co.clique.core.service.interfaces.UserService;
import za.co.clique.infrastructure.dao.db.entities.CarEntity;
import za.co.clique.infrastructure.dao.db.entities.RequestEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;
import za.co.clique.infrastructure.dao.db.repo.RequestRepository;
import za.co.clique.infrastructure.dao.db.repo.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository userRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    RequestService requestService;

    @Autowired
    DeviceService deviceService;


    public User authUser(String emailAddress, String password) throws ServiceException {

        try {
            UserEntity user = userRepository.authenticateUser(emailAddress, password);
            if(user == null) {
                throw new UserNotFoundException();
            }
                return getUserGraph(user.getUserId());
        }
        catch (DataAccessException e) {
            throw new ServiceException(e);
        }


    }

    @Override
    public User registerUser(User user) throws ServiceException {


            if (userRepository.userAlreadyRegistered(user.getEmailAddress())) {
                throw new UserAlreadyRegisteredException();
            } else {
                try {

                    UserEntity userEntity = new UserEntity();
                    userEntity.setEmailAddress(user.getEmailAddress());
                    userEntity.setName(user.getName());
                    userEntity.setPassword(user.getPassword());

                    List<CarEntity> carList = new ArrayList<>();
                    for(Car c: user.getRegisteredCars()) {
                        CarEntity aCar = new CarEntity();
                        aCar.setModel(c.getModel());
                        aCar.setYear(c.getYear());
                        aCar.setMake(c.getMake());
                        aCar.setUserEntity(userEntity);
                        carList.add(aCar);
                    }
                    Set<CarEntity> carEntitySet = new HashSet<>(carList);
                    userEntity.setCarEntitySet(carEntitySet);
                    user.setUserId(userRepository.save(userEntity).getUserId());

                    Device userDevice = new Device();
                    userDevice.setId(user.getUserId());
                    userDevice.setDeviceToken(user.getDeviceToken());
                    deviceService.registerDevice(userDevice);

                    return user;
                } catch (DataAccessException e) {
                    throw new ServiceException();
                }
                catch (UnableToCaptureDeviceException e) {
                    throw new UnableToCaptureDeviceException();
                }
            }


    }

    @Override
    public User getUserGraph(int userId) throws ServiceException {
        try {
            User userGraph = new User();
            UserEntity storedUser = userRepository.findOne(userId);
            userGraph.setUserId(storedUser.getUserId());
            userGraph.setName(storedUser.getName());
            userGraph.setEmailAddress(storedUser.getEmailAddress());
            userGraph.setRegisteredCars(getUsersCar(storedUser.getCarEntitySet()));

            Request inProgressRequest=  requestService.getActiveUserRequest(userId);

            if(inProgressRequest != null) {

            userGraph.setHasPendingRequest(true);
            userGraph.setCurrentRequest(inProgressRequest);

            }

            return userGraph;
        } catch (ServiceException e) {
            throw new UnableToGetRegisteredCarsException();
        } catch (Exception e) {
            throw new UnableToGetUserGraphException();
        }
    }

    private List<Car> getUsersCar(Set<CarEntity> carEntitySet) throws ServiceException {
        try {
            List<Car> carList = new ArrayList<>();
            for(CarEntity car: carEntitySet) {
                Car aCar = new Car();
           //     aCar.setUserId(car.getUserId());
                aCar.setMake(car.getMake());
                aCar.setYear(car.getYear());
                aCar.setModel(car.getModel());
                aCar.setCarId(car.getCarId());

                carList.add(aCar);
            }

            return carList;
        } catch (Exception e) {
            throw new UnableToGetRegisteredCarsException();
        }
    }
}
