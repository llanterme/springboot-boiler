package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Car;
import za.co.clique.core.domain.Category;
import za.co.clique.core.domain.User;
import za.co.clique.core.exceptions.*;
import za.co.clique.core.service.interfaces.CategoryService;
import za.co.clique.core.service.interfaces.UserService;
import za.co.clique.infrastructure.dao.db.entities.CarEntity;
import za.co.clique.infrastructure.dao.db.entities.CategoryEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;
import za.co.clique.infrastructure.dao.db.repo.CategoryRepository;
import za.co.clique.infrastructure.dao.db.repo.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class CategoryServiceImpl implements CategoryService {


    @Autowired
    CategoryRepository categoryRepository;


    @Override
    public List<Category> getCategories() throws ServiceException {
        try {

            List<Category> allCategories = new ArrayList<>();
            for(CategoryEntity dbCat: categoryRepository.findAll()) {
                Category aCategory = new Category();
                aCategory.setCategory(dbCat.getCategory());
                aCategory.setCategoryId(dbCat.getCategoryId());
                aCategory.setBaseRate(dbCat.getBaseRate());

                allCategories.add(aCategory);
            }

            return allCategories;

        } catch (DataAccessException e) {

            throw new ServiceException();
        }
    }

    @Override
    public Category getCategory(int categoryId) throws ServiceException {
        Category category = new Category();
        try {
            CategoryEntity dbCategory = categoryRepository.findOne(categoryId);
            category.setCategoryId(dbCategory.getCategoryId());
            category.setCategory(dbCategory.getCategory());
            category.setBaseRate(dbCategory.getBaseRate());

            return category;

        } catch (DataAccessException e) {
            throw new ServiceException("Unable to get a category");
        }
    }
}
