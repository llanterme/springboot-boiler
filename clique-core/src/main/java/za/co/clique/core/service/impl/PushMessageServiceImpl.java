package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Api;
import za.co.clique.core.domain.Push;
import za.co.clique.core.exceptions.ServiceException;
import za.co.clique.core.service.interfaces.ApiService;
import za.co.clique.core.service.interfaces.PushMessasgeService;
import za.co.clique.core.utils.PushyAPI;
import za.co.clique.infrastructure.dao.db.entities.ApiEntity;
import za.co.clique.infrastructure.dao.db.repo.ApiInfoRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class PushMessageServiceImpl implements PushMessasgeService {


    @Autowired
    ApiInfoRepository apiInfoRepository;

    @Override
    public Push testPush(Push pushDetails, String keyType) throws ServiceException {

        List<String> deviceTokens = new ArrayList<>();

        // Add your device tokens here
        deviceTokens.add(pushDetails.getDeviceToken());

        // Convert to String[] array
        String[] to = deviceTokens.toArray(new String[deviceTokens.size()]);

        // Optionally, send to a publish/subscribe topic instead
        // String to = '/topics/news';

        // Set payload (any object, it will be serialized to JSON)
        Map<String, String> payload = new HashMap<>();

        // Add "message" parameter to payload
        payload.put("message", "Subject");

        // iOS notification fields
        Map<String, Object> notification = new HashMap<>();

        notification.put("badge", pushDetails.getBadge());
        notification.put("sound", pushDetails.getSound());
        notification.put("body", pushDetails.getBody());

        // Prepare the push request
        PushyAPI.PushyPushRequest push = new PushyAPI.PushyPushRequest(payload, to, notification);

        try {
            // Try sending the push notification
            String key;
            if(keyType.equals("user")) {
                key = "c2bea7199bd8e04cd5dd900b68c29a0ed2a45882b14b638a98b2053f6f2677ae";
            } else {
                key = "f8686c93015567aa9749fdf1ac9ba669b340235f69123b05cd2435a15e570eeb";
            }
            PushyAPI.sendPush(push, key);

            return new Push(pushDetails.getBadge(), pushDetails.getSound(),pushDetails.getBody());
        }
        catch (Exception exc) {
            // Error, print to console
            System.out.println(exc.toString());
            throw new ServiceException(exc.toString());
        }

    }
}
