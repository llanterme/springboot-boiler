package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToCreateRequestException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToCreateRequestException() {
    }

    public UnableToCreateRequestException(String message) {
        super(message);
    }

    public UnableToCreateRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToCreateRequestException(Throwable cause) {
        super(cause);
    }
}
