package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Technician;
import za.co.clique.core.domain.TechnicianRating;
import za.co.clique.core.exceptions.ServiceException;
import za.co.clique.core.exceptions.TechnicianNotFoundException;
import za.co.clique.core.exceptions.TechnicianNotVerifiedException;
import za.co.clique.core.exceptions.UserNotFoundException;
import za.co.clique.core.service.interfaces.TechnicianService;
import za.co.clique.infrastructure.dao.db.entities.TechnicianEntity;
import za.co.clique.infrastructure.dao.db.entities.TechnicianRatingEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;
import za.co.clique.infrastructure.dao.db.repo.TechnicianRatingRepository;
import za.co.clique.infrastructure.dao.db.repo.TechnicianRepository;

@Service
public class TechnicianServiceImpl implements TechnicianService {

    @Autowired
    TechnicianRepository technicianRepository;

    @Autowired
    TechnicianRatingRepository technicianRatingRepository;

    @Override
    public Technician authenticationTechnician(String emailAddress, String password) throws ServiceException {

        try {
            TechnicianEntity technicianEntity = technicianRepository.authenticateTechnician(emailAddress, password);
            if(technicianEntity == null) {
                throw new TechnicianNotFoundException();
            }

            if(technicianEntity.getVerified().equals("No")) {
                throw new TechnicianNotVerifiedException();
            }

            Technician authenticatedTechnician = new Technician();
            authenticatedTechnician.setTelephone(technicianEntity.getTelephone());
            authenticatedTechnician.setHourRate(technicianEntity.getHourRate());
            authenticatedTechnician.setName(technicianEntity.getName());
            authenticatedTechnician.setEmailAddress(technicianEntity.getEmailAddress());
            authenticatedTechnician.setHourRate(technicianEntity.getHourRate());
            authenticatedTechnician.setTechnicianId(technicianEntity.getTechnicianId());

            return authenticatedTechnician;
        }
        catch (DataAccessException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Technician getTechnician(int technicianId) throws ServiceException {

        try {
            TechnicianEntity dbTechnician = technicianRepository.findOne(technicianId);

            Technician technician = new Technician();
            technician.setTechnicianId(dbTechnician.getTechnicianId());
            technician.setActive(dbTechnician.getActive());
            technician.setEmailAddress(dbTechnician.getEmailAddress());
            technician.setHourRate(dbTechnician.getHourRate());
            technician.setName(dbTechnician.getName());
            technician.setVerified(dbTechnician.getVerified());
            technician.setHourRate(dbTechnician.getHourRate());
            technician.setTelephone(dbTechnician.getTelephone());
            technician.setTechnicianRating(getTechnicianRating(technicianId) );

            return technician;

        } catch (DataAccessException e) {
            throw new ServiceException("unable to get technician details.");
        }
    }

    private TechnicianRating getTechnicianRating(int technicianId) throws ServiceException {

        try {

            TechnicianRating technicianRating = new TechnicianRating();
            TechnicianRatingEntity dbTechnicianRating = technicianRatingRepository.getTechnicianRating(technicianId);
            technicianRating.setTechnicianId(dbTechnicianRating.getTechnicianId());
            technicianRating.setRatingCount(dbTechnicianRating.getRatingCount());
            technicianRating.setUserCount(dbTechnicianRating.getUserCount());
            technicianRating.setTechnicianRatingId(dbTechnicianRating.getTechnicianRatingId());
            technicianRating.setStars(dbTechnicianRating.getRatingCount() / dbTechnicianRating.getUserCount());

            return technicianRating;

        } catch (DataAccessException e) {
            throw new ServiceException("Unable to get technicians rating.");
        }
    }
}
