package za.co.clique.core.service.interfaces;


import za.co.clique.core.domain.Category;
import za.co.clique.core.domain.User;
import za.co.clique.core.exceptions.ServiceException;

import java.util.List;

public interface CategoryService {

    List<Category> getCategories() throws ServiceException;

    Category getCategory(int categoryId) throws ServiceException;

}
