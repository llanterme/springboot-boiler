package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class TechnicianNotFoundException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public TechnicianNotFoundException() {
    }

    public TechnicianNotFoundException(String message) {
        super(message);
    }

    public TechnicianNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicianNotFoundException(Throwable cause) {
        super(cause);
    }
}
