package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToSaveCarServiceException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToSaveCarServiceException() {
    }

    public UnableToSaveCarServiceException(String message) {
        super(message);
    }

    public UnableToSaveCarServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToSaveCarServiceException(Throwable cause) {
        super(cause);
    }
}
