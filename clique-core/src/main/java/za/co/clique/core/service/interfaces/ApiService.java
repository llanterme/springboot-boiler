package za.co.clique.core.service.interfaces;


import za.co.clique.core.domain.Api;

public interface ApiService {

    Api getApiVersion();
}
