package za.co.clique.core.domain;

/**
 * Created by Luke on 6/19/17.
 */
public class CarModel {

    private int modelId;

    private String model;

    private int carWebId;

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCarWebId() {
        return carWebId;
    }

    public void setCarWebId(int carWebId) {
        this.carWebId = carWebId;
    }
}
