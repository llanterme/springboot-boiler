package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnablePersistModelsServiceException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnablePersistModelsServiceException() {
    }

    public UnablePersistModelsServiceException(String message) {
        super(message);
    }

    public UnablePersistModelsServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnablePersistModelsServiceException(Throwable cause) {
        super(cause);
    }
}
