package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Car;
import za.co.clique.core.domain.CarMakes;
import za.co.clique.core.domain.CarModel;
import za.co.clique.core.exceptions.ServiceException;
import za.co.clique.core.exceptions.UnablePersistModelsServiceException;
import za.co.clique.core.exceptions.UnableToGetCarMakesServiceException;
import za.co.clique.core.exceptions.UnableToSaveCarServiceException;
import za.co.clique.core.service.interfaces.CarService;
import za.co.clique.infrastructure.dao.db.entities.CarEntity;
import za.co.clique.infrastructure.dao.db.entities.MakeEntity;
import za.co.clique.infrastructure.dao.db.entities.ModelEntity;
import za.co.clique.infrastructure.dao.db.repo.CarMakeRepository;
import za.co.clique.infrastructure.dao.db.repo.CarModelRepository;
import za.co.clique.infrastructure.dao.db.repo.CarRepository;
import za.co.clique.infrastructure.dao.rest.entities.CarRestEntity;
import za.co.clique.infrastructure.dao.rest.interfaces.CarRestDao;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.util.ArrayList;
import java.util.List;


@Service
public class CarServiceImpl implements CarService {


    @Autowired
    CarRestDao carRestDao;

    @Autowired
    CarMakeRepository carMakeRepository;

    @Autowired
    CarModelRepository carModelRepository;

    @Autowired
    CarRepository carRepository;

    @Override
    public String persistCarMakes() throws ServiceException {
        try {

            List<CarMakes> rawCarsList = getCarMakesFromRest();
            for (CarMakes make: getCarMakesFromRest()) {
                MakeEntity newMake = new MakeEntity();
                newMake.setName(make.getMake());
                newMake.setWebId(make.getCarMakeWebId());

                carMakeRepository.save(newMake);

           }
            return "Persisted " + rawCarsList.size() + " car makes.";
        } catch (ServiceException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void persistCarModels() throws ServiceException {

        try {

            List<CarMakes> carDbMakes = getCarMakesFromDb();
            if(carDbMakes != null) {

               for (CarMakes makes : carDbMakes) {
                  List<CarRestEntity> modelsList = carRestDao.getCarModels(makes.getCarMakeWebId());
                    saveCarModels(modelsList, makes.getMakeId());
                }
            }


        } catch (DaoException e) {
            throw new ServiceException();
        } catch (UnablePersistModelsServiceException e) {
         throw new UnablePersistModelsServiceException();
        }

    }

    @Override
    public List<CarMakes> getCarMakesFromDb() throws ServiceException {

        try {
            List<CarMakes> dbCarMakesList = new ArrayList<>();
            for(MakeEntity makes: carMakeRepository.findAll()) {
                CarMakes aCarMake = new CarMakes();
                aCarMake.setMakeId(makes.getMakeId());
                aCarMake.setMake(makes.getName());
                aCarMake.setCarMakeWebId(makes.getWebId());


                dbCarMakesList.add(aCarMake);
            }

            return dbCarMakesList;
        } catch (Exception e) {
            throw new ServiceException("Unable to retrive list of car makes from db");
        }

    }

    @Override
    public List<CarModel> getCarModelsFromDb(int makeId) throws ServiceException {

        try {
            List<CarModel> dbCarModelList = new ArrayList<>();
            for(ModelEntity models: carModelRepository.findAllCarModels(makeId)) {
                CarModel aCarModel = new CarModel();
                aCarModel.setModelId(models.getModelId());
                aCarModel.setModel(models.getName());

                dbCarModelList.add(aCarModel);
            }

            return dbCarModelList;
        } catch (Exception e) {
            throw new ServiceException("Unable to retrive list of car models from db");
        }

    }

    protected List<CarMakes> getCarMakesFromRest() throws ServiceException {
        List<CarMakes> carMakesList = new ArrayList<>();
        try {

            List<CarRestEntity> allMakesRaw = carRestDao.getCarMakes();

            for (CarRestEntity makes : allMakesRaw) {
                CarMakes aCarMake = new CarMakes();
                aCarMake.setCarMakeWebId(makes.getValue());
                aCarMake.setMake(makes.getName());

                carMakesList.add(aCarMake);
            }

            return carMakesList;


        } catch (DaoException e) {
            throw new UnableToGetCarMakesServiceException();
        }
    }

    protected void saveCarModels(List<CarRestEntity> modelsList,  int carMakeId) throws ServiceException {

        try {

            for(CarRestEntity model: modelsList) {
                ModelEntity newModel = new ModelEntity();
                newModel.setName(model.getName());
                newModel.setWebId(model.getValue());
                newModel.setMakeId(carMakeId);
                carModelRepository.save(newModel);
            }

        } catch (Exception e) {
                throw new UnablePersistModelsServiceException();
        }

    }

    @Override
    public int addUserCar(Car car) throws ServiceException {
        try {
            CarEntity newCar = new CarEntity();
            newCar.setMake(car.getMake());
            newCar.setModel(car.getModel());
            newCar.setYear(car.getYear());
       //     newCar.setUserId(car.getUserId());
            CarEntity savedCar = carRepository.saveAndFlush(newCar);
            return savedCar.getCarId();
        } catch (DataAccessException e) {
            throw new UnableToSaveCarServiceException();
        }
    }

    @Override
    public Car getCar(int carId) throws ServiceException {
        Car car = new Car();
        try {
            CarEntity dbCar = carRepository.findOne(carId);
            car.setCarId(dbCar.getCarId());
            car.setMake(dbCar.getMake());
            car.setModel(dbCar.getModel());
            car.setUserId(dbCar.getCarId());
            car.setYear(dbCar.getYear());

            return car;
        } catch (DataAccessException e) {
            throw new ServiceException("unable to get car");
        }
    }
}
