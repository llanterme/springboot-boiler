package za.co.clique.core.service.interfaces;

import za.co.clique.core.domain.Request;
import za.co.clique.core.domain.Response;
import za.co.clique.core.exceptions.ServiceException;

import java.util.List;

public interface ResponseService {

    List<Response> getTechnicianResponses(int requestId) throws ServiceException;

    Response selectTechnicianForJob(int responseId) throws ServiceException;

    Response getResponse(int responseId) throws ServiceException;

    void deleteResponses(int requestId) throws ServiceException;

    Response createResponse(Response technicianResponse) throws ServiceException;

}
