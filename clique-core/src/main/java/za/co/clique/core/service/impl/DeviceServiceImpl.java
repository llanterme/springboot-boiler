package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Api;
import za.co.clique.core.domain.Device;
import za.co.clique.core.exceptions.ServiceException;
import za.co.clique.core.exceptions.UnableToCaptureDeviceException;
import za.co.clique.core.service.interfaces.ApiService;
import za.co.clique.core.service.interfaces.DeviceService;
import za.co.clique.infrastructure.dao.db.entities.ApiEntity;
import za.co.clique.infrastructure.dao.db.entities.DeviceEntity;
import za.co.clique.infrastructure.dao.db.repo.ApiInfoRepository;
import za.co.clique.infrastructure.dao.db.repo.DeviceRepository;


@Service
public class DeviceServiceImpl implements DeviceService {


    @Autowired
    DeviceRepository deviceRepository;

    @Override
    public Device registerDevice(Device device) throws ServiceException {
        try {

            DeviceEntity newDevice = new DeviceEntity();
            newDevice.setDeviceToken(device.getDeviceToken());
            newDevice.setId(device.getId());

            int deviceId = deviceRepository.save(newDevice).getDeviceId();

            device.setId(deviceId);

            return device;

        } catch (DataAccessException e) {
            throw new UnableToCaptureDeviceException();
        }
    }

    @Override
    public Device getUserDevice(int Id) throws ServiceException {
        Device userDevice = new Device();
        try {
        DeviceEntity deviceEntity = deviceRepository.getUserDevice(Id);
            userDevice.setDeviceToken(deviceEntity.getDeviceToken());
            userDevice.setId(deviceEntity.getId());
            userDevice.setDeviceId(deviceEntity.getDeviceId());

            return userDevice;


        } catch (DataAccessException e) {
            throw new ServiceException("Unable to get users device information.");
        }
    }
}
