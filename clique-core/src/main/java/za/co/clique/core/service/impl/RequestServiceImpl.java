package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Request;
import za.co.clique.core.domain.Technician;
import za.co.clique.core.exceptions.*;
import za.co.clique.core.service.interfaces.*;
import za.co.clique.core.utils.Utils;
import za.co.clique.infrastructure.dao.db.entities.RequestEntity;
import za.co.clique.infrastructure.dao.db.repo.RequestRepository;
import za.co.clique.infrastructure.dao.db.repo.ResponseRepository;
import za.co.clique.infrastructure.dao.db.repo.UserRepository;
import za.co.clique.infrastructure.dao.rest.entities.DistanceMatrixEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class RequestServiceImpl implements RequestService {


    @Autowired
    RequestRepository requestRepository;

    @Autowired
    CarService carService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ResponseService responseService;

    @Autowired
    ResponseRepository responseRepository;


    @Override
    public List<Request> getActiveRequests(String technicianLocation) throws ServiceException {
        List<Request> requests = new ArrayList<>();
        DistanceMatrixEntity distanceMatrixEntity = new DistanceMatrixEntity();
        try {

            for (RequestEntity dbRequest: requestRepository.getRequestsPerStatus("Open")) {

                //How far is the user from the technician

                try {
                    distanceMatrixEntity = Utils.getDistanceMatrix(dbRequest.getCoords(), technicianLocation);
                } catch (UnableToGetDistanceAndDurationException e) {

                }

                    // TODO let the technician decide how far out he wants to filter
//                if(Double.parseDouble(distanceMatrixEntity.getDistance().split(" ")[0]) < 10) {
                    Request aRequest = buildRequest(dbRequest);

                    aRequest.setUserToTechnicianDistance(distanceMatrixEntity.getDistance());
                    aRequest.setUserToTechnicianDuration(distanceMatrixEntity.getDuration());
                    requests.add(aRequest);
  //              }
            }

            return requests;
        }catch (DataAccessException e) {
            throw new ServiceException("Unable to get a list of requests from database.");
        }
        catch (ServiceException e) {
            throw new ServiceException(e.getMessage());
        }

        }

    @Override
    public Request createRequest(Request request) throws ServiceException {

        try {

            if(requestRepository.requestAlreadyLogged(request.getUserId())) {
                throw new RequesAlreadyPendingException();
            }

            String dateLogged = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

            RequestEntity newRequest = new RequestEntity();
            newRequest.setCategoryId(request.getCategoryId());
            newRequest.setCoords(request.getCoords());
            newRequest.setNotes(request.getNotes());
            newRequest.setUserId(request.getUserId());
            newRequest.setRequestTime(dateLogged);
            newRequest.setStatus(request.getStatus());
            newRequest.setCarId(request.getCarId());
            newRequest.setAddress(request.getAddress());

            request.setRequestId(requestRepository.save(newRequest).getRequestId());
            request.setRequestTime(dateLogged);
            request.setCar(carService.getCar(request.getCarId()));
            request.setCategory(categoryService.getCategory(request.getCategoryId()));


            return request;

        } catch (DataAccessException e) {
            throw new UnableToCreateRequestException();

        }

    }

    @Override
    public Request getRequest(int requestId) throws ServiceException {

        try {
            RequestEntity dbReqest = requestRepository.findOne(requestId);

            return buildRequest(dbReqest);
        } catch (DataAccessException e) {
            throw new ServiceException("Unable to get request details.");
        }
    }

    @Override
    public Request getActiveUserRequest(int userId) throws ServiceException {

        try {
            RequestEntity dbReqest = requestRepository.getUserRequest(userId);

            if(dbReqest != null) {
                return buildRequest(dbReqest);
            } else {
                return null;
            }


        } catch (DataAccessException e) {
            throw new ServiceException("Unable to get active requests details for user.");
        }
    }

    @Override
    public Request acceptRequest(int requestId, int responseId) throws ServiceException {

        try {

            // Update Request
            RequestEntity dbRequest = requestRepository.findOne(requestId);
            dbRequest.setStatus("Assigned");
            requestRepository.save(dbRequest);

            Request currentRequest = getRequest(requestId);
            currentRequest.setStatus(dbRequest.getStatus());

            //Update Response
            responseService.selectTechnicianForJob(responseId);

            //Delete Responses
            responseService.deleteResponses(requestId);



            return currentRequest;

        } catch (DataAccessException e) {
            throw new UnableToCreateRequestException();

        }


    }

    private Request buildRequest(RequestEntity dbRequest) throws ServiceException {

        Request requestDetails = new Request();
        try {

            requestDetails.setRequestId(dbRequest.getRequestId());
            requestDetails.setCategoryId(dbRequest.getCategoryId());
            requestDetails.setAddress(dbRequest.getAddress());
            requestDetails.setCarId(dbRequest.getCarId());
            requestDetails.setCoords(dbRequest.getCoords());
            requestDetails.setUserId(dbRequest.getUserId());
            requestDetails.setNotes(dbRequest.getNotes());
            requestDetails.setRequestTime(dbRequest.getRequestTime());
            requestDetails.setStatus(dbRequest.getStatus());
            requestDetails.setCar(carService.getCar(dbRequest.getCarId()));
            requestDetails.setCategory(categoryService.getCategory(dbRequest.getCategoryId()));

            return requestDetails;
        } catch (DataAccessException e) {
            throw new ServiceException("Unable to get request details.");
        }

    }
}
