package za.co.clique.core.domain;

import java.sql.Date;
import java.util.List;

public class User {

    private int userId;
    private String name;
    private String emailAddress;
    private String password;
    private String deviceToken;
    private List<Car> registeredCars;
    private boolean hasPendingRequest;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    private Request currentRequest;

    public Request getCurrentRequest() {
        return currentRequest;
    }

    public void setCurrentRequest(Request currentRequest) {
        this.currentRequest = currentRequest;
    }

    public boolean isHasPendingRequest() {
        return hasPendingRequest;
    }

    public void setHasPendingRequest(boolean hasPendingRequest) {
        this.hasPendingRequest = hasPendingRequest;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Car> getRegisteredCars() {
        return registeredCars;
    }

    public void setRegisteredCars(List<Car> registeredCars) {
        this.registeredCars = registeredCars;
    }
}
