package za.co.clique.core.service.interfaces;

import za.co.clique.core.domain.Car;
import za.co.clique.core.domain.CarMakes;
import za.co.clique.core.domain.CarModel;
import za.co.clique.core.exceptions.ServiceException;

import java.util.List;

public interface CarService {

    String persistCarMakes() throws ServiceException;

    void persistCarModels() throws ServiceException;

    List<CarMakes> getCarMakesFromDb() throws ServiceException;

    List<CarModel> getCarModelsFromDb(int makeId) throws ServiceException;

    int addUserCar(Car car) throws ServiceException;

    Car getCar(int carId) throws ServiceException;

}
