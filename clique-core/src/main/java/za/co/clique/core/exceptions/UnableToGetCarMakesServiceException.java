package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToGetCarMakesServiceException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToGetCarMakesServiceException() {
    }

    public UnableToGetCarMakesServiceException(String message) {
        super(message);
    }

    public UnableToGetCarMakesServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToGetCarMakesServiceException(Throwable cause) {
        super(cause);
    }
}
