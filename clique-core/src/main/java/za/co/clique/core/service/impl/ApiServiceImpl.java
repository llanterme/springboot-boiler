package za.co.clique.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.clique.core.domain.Api;
import za.co.clique.core.service.interfaces.ApiService;
import za.co.clique.infrastructure.dao.db.entities.ApiEntity;
import za.co.clique.infrastructure.dao.db.repo.ApiInfoRepository;


@Service
public class ApiServiceImpl implements ApiService {


    @Autowired
    ApiInfoRepository apiInfoRepository;

    @Override
    public Api getApiVersion() {

        ApiEntity dbRawEntity = apiInfoRepository.findOne(3);
        return new Api(dbRawEntity.getApiVersion());
    }
}
