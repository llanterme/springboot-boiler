package za.co.clique.core.exceptions;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToCaptureDeviceException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToCaptureDeviceException() {
    }

    public UnableToCaptureDeviceException(String message) {
        super(message);
    }

    public UnableToCaptureDeviceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToCaptureDeviceException(Throwable cause) {
        super(cause);
    }
}
