package za.co.clique.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.service.interfaces.ApiService;
import za.co.clique.server.config.ApplicationConfig;
import za.co.clique.server.security.JWTTokenNeeded;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

@Component
@Path("/api")
public class ApiInfoResource extends AbstractResource{

    @Autowired
    ApiService apiService;

    @Autowired
    private ApplicationConfig applicationConfig;

    @GET
    @Path("/version")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventVersion() {
        return Response.ok().entity(apiService.getApiVersion()).build();
    }

    @GET
    @Path("/debug")
    @JWTTokenNeeded
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDebugStatus() {
        return Response.ok().entity(applicationConfig.getDebug()).build();
    }
}
