package za.co.clique.server.error;




import org.apache.commons.lang3.exception.ExceptionUtils;
import za.co.clique.core.exceptions.*;

import javax.ws.rs.core.Response;

public class ExceptionHandlingUtil {

    private static ExceptionHandlingUtil instance = new ExceptionHandlingUtil();

    public static ExceptionHandlingUtil get() {
        return instance;
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public CustomWebApplicationException handleAndGetExceptionFromResource(Exception e, Class<?> resource,
                                                                           String method, Object contextObject) {

        CustomWebApplicationException mappedException = e instanceof CustomWebApplicationException ? (CustomWebApplicationException) e : mapException(e);

        return mappedException;
    }


    protected CustomWebApplicationException mapException(Exception e) {

        if (e instanceof ApiNotConfiguredException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("001", "Api not configured.", e), e);
        }

        if (e instanceof UnableToGetCarMakesServiceException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("002", "Unable to get car nakes.", e), e);
        }

        if (e instanceof UnablePersistModelsServiceException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("003", "Unable to get persist car models.", e), e);
        }

        if (e instanceof UnableToSaveCarServiceException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("004", "Unable to persist users car.", e), e);
        }

        if (e instanceof UnableToGetRegisteredCarsException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("005", "Unable to get users car list.", e), e);
        }

        if (e instanceof UnableToGetUserGraphException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("005", "Unable to get users graph.", e), e);
        }

        if (e instanceof UserAlreadyRegisteredException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("006", "User already registered.", e), e);
        }

        if (e instanceof UserNotFoundException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("007", "User not found.", e), e);
        }

        if (e instanceof UnableToCreateRequestException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("008", "Unable to save request.", e), e);
        }

        if (e instanceof RequesAlreadyPendingException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("009", "Request already pending.", e), e);
        }

        if (e instanceof TechnicianNotFoundException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("010", "Technician not found.", e), e);
        }

        if (e instanceof TechnicianNotVerifiedException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("011", "You are not yet verified.", e), e);
        }

        if (e instanceof TechnicianAlreadyRespondedException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("012", "You have already respodned to this request.", e), e);
        }

        if (e instanceof UnableToCaptureDeviceException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("013", "Unable to capture users device information.", e), e);
        }


        return new CustomWebApplicationException(Response.Status.INTERNAL_SERVER_ERROR,
                getErrorMessage("999", "Unexpected error.", e), e);

    }


    protected ErrorMessage getErrorMessage(String code, String message, Exception e) {
        return new ErrorMessage(code, message, ExceptionUtils.getStackTrace(e));
    }





}
