package za.co.clique.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.service.interfaces.ResponseService;
import za.co.clique.server.error.ExceptionHandlingUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/response")
public class ResponseResource extends AbstractResource{


    @Autowired
    ResponseService responseService;

    @GET
    @Path("/all/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getResponses(@PathParam("requestId") String requestId) {

        try {
            return Response.ok().entity(responseService.getTechnicianResponses(Integer.parseInt(requestId))).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, ResponseResource.class, "getResponses", null);
        }

    }


    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createResponse(za.co.clique.core.domain.Response response) {

        try {
            return Response.ok().entity(responseService.createResponse(response)).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, RequestResource.class, "createResponse", null);
        }

    }

}
