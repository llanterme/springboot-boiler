package za.co.clique.server.resources;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.service.interfaces.CarService;
import za.co.clique.core.service.interfaces.CategoryService;
import za.co.clique.server.error.ExceptionHandlingUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/category")
public class CategoryResource extends AbstractResource {

    @Autowired
    CategoryService categoryService;

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategories() {

        try {
            return Response.ok().entity(categoryService.getCategories()).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, CategoryResource.class, "getCategories", null);
        }
    }


}
