package za.co.clique.server.resources;


import za.co.clique.server.security.KeyGenerator;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AbstractResource {

    @Context
    public UriInfo uriInfo;

    @Context
    public HttpHeaders headers;

    @Inject
    public KeyGenerator keyGenerator;
}
