package za.co.clique.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.domain.Car;
import za.co.clique.core.domain.Request;
import za.co.clique.core.domain.User;
import za.co.clique.core.service.interfaces.CarService;
import za.co.clique.core.service.interfaces.RequestService;
import za.co.clique.core.service.interfaces.UserService;
import za.co.clique.server.error.ExceptionHandlingUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/request")
public class RequestResource extends AbstractResource{


    @Autowired
    RequestService requestService;

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRequest(Request request) {

        try {
            return Response.ok().entity(requestService.createRequest(request)).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, RequestResource.class, "createRequest", null);
        }

    }

    @GET
    @Path("/find/all/{technicianCurrentLocation}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOpenRequests(@PathParam("technicianCurrentLocation") String technicianCurrentLocation) {

        try {
            return Response.ok().entity(requestService.getActiveRequests(technicianCurrentLocation)).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, ResponseResource.class, "getOpenRequests", null);
        }

    }

    @GET
    @Path("/find/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserActiveRequest(@PathParam("userId") String userId) {

        try {
            return Response.ok().entity(requestService.getActiveUserRequest(Integer.parseInt(userId))).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, ResponseResource.class, "getUserActiveRequest", null);
        }

    }

    @GET
    @Path("/accept")
    public Response acceptRequest(@QueryParam("requestId") String requestId, @QueryParam("responseId") String responseId) {

        try {
            return Response.ok().entity(requestService.acceptRequest(Integer.parseInt(requestId), Integer.parseInt(responseId))).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, RequestResource.class, "acceptRequest", null);
        }
    }


}
