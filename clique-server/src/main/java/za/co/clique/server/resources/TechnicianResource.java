package za.co.clique.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.service.interfaces.ResponseService;
import za.co.clique.core.service.interfaces.TechnicianService;
import za.co.clique.server.error.ExceptionHandlingUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/technician")
public class TechnicianResource extends AbstractResource{


    @Autowired
    TechnicianService technicianService;

    @GET
    @Path("/find/{technicianId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTechnician(@PathParam("technicianId") String technicianId) {

        try {
            return Response.ok().entity(technicianService.getTechnician(Integer.parseInt(technicianId))).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, TechnicianResource.class, "getTechnician", null);
        }

    }

}
