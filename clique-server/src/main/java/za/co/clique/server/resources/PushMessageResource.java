package za.co.clique.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.domain.Push;
import za.co.clique.core.domain.User;
import za.co.clique.core.service.interfaces.ApiService;
import za.co.clique.core.service.interfaces.PushMessasgeService;
import za.co.clique.server.config.ApplicationConfig;
import za.co.clique.server.error.ExceptionHandlingUtil;
import za.co.clique.server.security.JWTTokenNeeded;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/push")
public class PushMessageResource extends AbstractResource{

    @Autowired
    PushMessasgeService pushMessasgeService;

    @Autowired
    private ApplicationConfig applicationConfig;

    @POST
    @Path("test")
    public Response testPush(Push pushMessage) {
        try{

            return Response.ok().entity("").build();
        } catch (Exception e) {

            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "authUser", null);
        }

    }


}
