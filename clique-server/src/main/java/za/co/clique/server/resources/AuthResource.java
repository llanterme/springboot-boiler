package za.co.clique.server.resources;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.domain.Technician;
import za.co.clique.core.domain.User;
import za.co.clique.core.service.interfaces.TechnicianService;
import za.co.clique.core.service.interfaces.UserService;
import za.co.clique.server.error.ExceptionHandlingUtil;
import za.co.clique.server.security.KeyGenerator;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.*;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;


@Component
@Path("/authenticate")
public class AuthResource extends AbstractResource {


    @Autowired
    UserService userService;

    @Autowired
    TechnicianService technicianService;

    @POST
    @Path("authUser")
    public Response authUser(User user) {
        try{
            User authenticatedUser = userService.authUser(user.getEmailAddress(), user.getPassword());

            return Response.ok().entity(authenticatedUser).build();
        } catch (Exception e) {

            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "authUser", null);
        }

    }

    @POST
    @Path("authTechnician")
    public Response authTechnician(Technician technician) {
        try{

            return Response.ok().entity(technicianService.authenticationTechnician(technician.getEmailAddress(),technician.getPassword())).build();
        } catch (Exception e) {

            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "authTechnician", null);
        }

    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser() {

        try {

            //Get Authorization Header
            String authString = headers.getRequestHeader("Authorization").get(0);


            String basicAuthEncoded = authString.substring(6);
            String basicAuthString = new String(Base64.decodeBase64(basicAuthEncoded.getBytes()));
            String[] usernameAndPassword = basicAuthString.split(":");



            // Authenticate the user using the credentials provided
            //  authenticate(login, password);

            // Issue a token for the user
            String token = issueToken(basicAuthString);

            // Return the token on the response
            return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();

        } catch (Exception e) {
            return Response.status(UNAUTHORIZED).build();
        }
    }


    private String issueToken(String login) {
        Key key = keyGenerator.generateKey();
        String jwtToken = Jwts.builder()
                .setSubject(login)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        //logger.info("#### generating token for a key : " + jwtToken + " - " + key);
        return jwtToken;

    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
