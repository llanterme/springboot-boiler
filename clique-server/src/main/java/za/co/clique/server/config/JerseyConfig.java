package za.co.clique.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import za.co.clique.server.security.JWTTokenNeededFilter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Component
@ApplicationPath("/v1")
public class JerseyConfig extends ResourceConfig {

    @Autowired
    public JerseyConfig(ObjectMapper objectMapper) {
        // register endpoints
        packages("za.co.clique.server.resources");
        // register jackson for json
        register(new ObjectMapperContextResolver(objectMapper));

        register(
                new LoggingFeature(
                        java.util.logging.Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
                        java.util.logging.Level.SEVERE,
                        LoggingFeature.Verbosity.PAYLOAD_ANY,
                        Integer.MIN_VALUE)
        );

        register(new JWTTokenNeededFilter());
    }

    @Provider
    public static class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

        private final ObjectMapper mapper;

        public ObjectMapperContextResolver(ObjectMapper mapper) {
            this.mapper = mapper;
        }

        @Override
        public ObjectMapper getContext(Class<?> type) {
            return mapper;
        }
    }
}