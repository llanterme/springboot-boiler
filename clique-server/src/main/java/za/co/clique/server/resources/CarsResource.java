package za.co.clique.server.resources;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.service.interfaces.CarService;
import za.co.clique.server.error.ExceptionHandlingUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/cars")
public class CarsResource extends AbstractResource {

    @Autowired
    CarService carService;

    @GET
    @Path("/updatemakes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMakes() {

        try {
            return Response.ok().entity(carService.persistCarMakes()).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, CarsResource.class, "updateMakes", null);
        }
    }

    @GET
    @Path("/updatemodels")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateModels() {

        try {
           carService.persistCarModels();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, CarsResource.class, "updateModels", null);
        }
    }

    @GET
    @Path("/models/{makeid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarModels(@PathParam("makeid") String makeid) {

        try {
            return Response.ok().entity(carService.getCarModelsFromDb(Integer.valueOf(makeid))).build();

        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, CarsResource.class, "getCarModels", null);
        }
    }

    @GET
    @Path("/makes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarMakes() {

        try {
            return Response.ok().entity(carService.getCarMakesFromDb()).build();

        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, CarsResource.class, "getCarMakes", null);
        }
    }
}
