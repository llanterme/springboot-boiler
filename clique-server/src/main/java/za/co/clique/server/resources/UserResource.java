package za.co.clique.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.clique.core.domain.Car;
import za.co.clique.core.domain.User;
import za.co.clique.core.service.interfaces.CarService;
import za.co.clique.core.service.interfaces.UserService;
import za.co.clique.server.error.ExceptionHandlingUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/user")
public class UserResource extends AbstractResource{

    @Autowired
    CarService carService;

    @Autowired
    UserService userService;

    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addOrUpdateUser(User user) {

        try {
            return Response.ok().entity(userService.registerUser(user)).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "addOrUpdate", null);
        }

    }

    @POST
    @Path("/addCar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUsersCar(Car car) {

        try {
            return Response.ok().entity(carService.addUserCar(car)).build();
        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "addUsersCar", null);
        }

    }


    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserGraph(@PathParam("userId") String userId) {

        try {
            return Response.ok().entity(userService.getUserGraph(Integer.valueOf(userId))).build();

        } catch (Exception e) {
            throw ExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "getUserGraph", null);
        }
    }
}
