package za.co.clique.infrastructure.dao.rest.entities;

public class DistanceMatrixEntity {

    private String distance;
    private String duration;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


    public DistanceMatrixEntity(String distance, String duration) {
        this.distance = distance;
        this.duration = duration;
    }

    public DistanceMatrixEntity() {
    }
}
