package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.RequestEntity;
import za.co.clique.infrastructure.dao.db.entities.TechnicianEntity;
import za.co.clique.infrastructure.dao.db.entities.TechnicianRatingEntity;

@Repository
@Transactional
public interface TechnicianRatingRepository extends JpaRepository<TechnicianRatingEntity, Integer> {


    @Query("SELECT p FROM TechnicianRatingEntity p WHERE p.technicianId = :technicianId")
    TechnicianRatingEntity getTechnicianRating(@Param("technicianId") int technicianId);


}
