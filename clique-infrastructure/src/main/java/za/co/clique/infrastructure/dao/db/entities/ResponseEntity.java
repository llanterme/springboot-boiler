package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;

/**
 * Created by Luke on 9/18/17.
 */
@Entity
@Table(name = "response", schema = "clique", catalog = "")
public class ResponseEntity {
    private int responseId;
    private int requestId;
    private int technicianId;
    private String responseTime;
    private String lastLocation;
    private String status;

    @Id
    @Column(name = "response_id")
    public int getResponseId() {
        return responseId;
    }

    public void setResponseId(int responseId) {
        this.responseId = responseId;
    }

    @Basic
    @Column(name = "request_id")
    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "technician_id")
    public int getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(int technicianId) {
        this.technicianId = technicianId;
    }

    @Basic
    @Column(name = "response_time")
    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    @Basic
    @Column(name = "last_location")
    public String getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(String lastLocation) {
        this.lastLocation = lastLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResponseEntity that = (ResponseEntity) o;

        if (responseId != that.responseId) return false;
        if (requestId != that.requestId) return false;
        if (technicianId != that.technicianId) return false;
        if (responseTime != that.responseTime) return false;
        if (lastLocation != null ? !lastLocation.equals(that.lastLocation) : that.lastLocation != null) return false;

        return true;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int result = responseId;
        result = 31 * result + requestId;
        result = 31 * result + technicianId;
        result = 31 * result + (responseTime != null ? responseTime.hashCode() : 0);
        result = 31 * result + (lastLocation != null ? lastLocation.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
