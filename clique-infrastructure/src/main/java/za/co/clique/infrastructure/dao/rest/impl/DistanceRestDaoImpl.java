package za.co.clique.infrastructure.dao.rest.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import za.co.clique.infrastructure.dao.rest.entities.DistanceMatrixEntity;
import za.co.clique.infrastructure.dao.rest.handlers.RestTemplateResponseErrorHandler;
import za.co.clique.infrastructure.dao.rest.interfaces.DistanceRestDao;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.util.HashMap;

@Repository
public class DistanceRestDaoImpl implements DistanceRestDao {

    public DistanceMatrixEntity getTechnicianDistanceMatrix(HashMap<String, String> coOrds) throws DaoException {

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + coOrds.get("userLat") + "," + coOrds.get("userLong") + "&destinations=" + coOrds.get("techlat") + "," + coOrds.get("techLong") + "&key=AIzaSyDrVhOcvYYG5f2z7mvMX-RNJ3TniloVRFE";

        HttpEntity<?> httpEntity = new HttpEntity<>(requestHeaders);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        try {

            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);


            //TODO - Check for nulls
            JSONObject obj = new JSONObject(responseEntity.getBody());
            JSONArray rows = obj.getJSONArray("rows");
            JSONObject elementsObject = rows.getJSONObject(0);
            JSONArray elements = elementsObject.getJSONArray("elements");
            JSONObject distanceObject = elements.getJSONObject(0);
            JSONObject distance = distanceObject.getJSONObject("distance");

            JSONObject durationObject = elements.getJSONObject(0);
            JSONObject duration = durationObject.getJSONObject("duration");

            return new DistanceMatrixEntity(distance.getString("text"), duration.getString("text"));

        } catch (Exception e) {

            throw new DaoException();
        }

    }


}
