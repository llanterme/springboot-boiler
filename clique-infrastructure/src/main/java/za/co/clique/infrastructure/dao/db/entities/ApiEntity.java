package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;

@Entity
@Table(name = "api_info")
public class ApiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="api_info_id")
    private Integer apiInfoId;

    @Column(name="api_version")
    private String apiVersion;


    public Integer getApiInfoId() {
        return apiInfoId;
    }

    public void setApiInfoId(Integer apiInfoId) {
        this.apiInfoId = apiInfoId;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
