package za.co.clique.infrastructure.dao.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.CarEntity;

@Repository
@Transactional
public interface CarRepository extends JpaRepository<CarEntity, Integer> {



}
