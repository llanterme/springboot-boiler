package za.co.clique.infrastructure.dao.rest.entities;

/**
 * Created by Luke on 6/19/17.
 */
public class CarRequest {


    private String knownCategoryValues;

    private String category;

    public String getKnownCategoryValues() {
        return knownCategoryValues;
    }

    public void setKnownCategoryValues(String knownCategoryValues) {
        this.knownCategoryValues = knownCategoryValues;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public CarRequest(String knownCategoryValues, String category) {
        this.knownCategoryValues = knownCategoryValues;
        this.category = category;
    }

    public CarRequest() {
    }
}
