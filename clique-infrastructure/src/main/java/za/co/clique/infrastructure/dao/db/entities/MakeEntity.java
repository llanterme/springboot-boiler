package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;

/**
 * Created by Luke on 6/20/17.
 */
@Entity
@Table(name = "make", schema = "clique", catalog = "")
public class MakeEntity {
    private int makeId;
    private String name;
    private int webId;

    @Id
    @Column(name = "make_id")
    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "web_id")
    public int getWebId() {
        return webId;
    }

    public void setWebId(int webId) {
        this.webId = webId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MakeEntity that = (MakeEntity) o;

        if (makeId != that.makeId) return false;
        if (webId != that.webId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = makeId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + webId;
        return result;
    }
}
