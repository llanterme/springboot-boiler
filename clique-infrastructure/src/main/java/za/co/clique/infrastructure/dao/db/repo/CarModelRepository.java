package za.co.clique.infrastructure.dao.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.ModelEntity;

import java.util.List;


@Repository
@Transactional
public interface CarModelRepository extends JpaRepository<ModelEntity, Integer> {

    @Query("SELECT p FROM ModelEntity p WHERE p.makeId = :makeId")
    List<ModelEntity> findAllCarModels(@Param("makeId") int makeId);

}

