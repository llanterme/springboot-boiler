package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.CategoryEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;

@Repository
@Transactional
public interface CategoryRepository extends JpaRepository<CategoryEntity, Integer> {



}
