package za.co.clique.infrastructure.exceptions;


public class DaoException extends Exception {

    private static final long serialVersionUID = -8507007009649139382L;

    public DaoException() {
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }



    public DaoException(Throwable cause) {
        super(cause);
    }

}