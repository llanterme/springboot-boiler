package za.co.clique.infrastructure.dao.rest.interfaces;

import za.co.clique.infrastructure.dao.rest.entities.CarRestEntity;
import za.co.clique.infrastructure.dao.rest.entities.DistanceMatrixEntity;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.util.HashMap;
import java.util.List;

public interface CarRestDao {

    List<CarRestEntity> getCarMakes() throws DaoException;

    List<CarRestEntity> getCarModels(int webId) throws DaoException;

}
