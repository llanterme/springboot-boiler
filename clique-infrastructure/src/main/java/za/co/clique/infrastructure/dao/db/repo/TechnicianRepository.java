package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.TechnicianEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;

@Repository
@Transactional
public interface TechnicianRepository extends JpaRepository<TechnicianEntity, Integer> {


    @Query("SELECT p FROM TechnicianEntity p WHERE LOWER(p.emailAddress) = LOWER(:emailAddress) AND LOWER(p.password) = LOWER(:password)")
    TechnicianEntity authenticateTechnician(@Param("emailAddress") String emailAddress, @Param("password") String password);



}
