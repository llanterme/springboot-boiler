package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Luke on 8/15/17.
 */
@Entity
@Table(name = "user", schema = "clique", catalog = "")
public class UserEntity {
    private int userId;
    private String name;
    private String emailAddress;
    private String password;
    private Set<CarEntity> carEntitySet;

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Column(name = "email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userEntity" , cascade = { CascadeType.ALL } )
    public Set<CarEntity> getCarEntitySet() {
        return carEntitySet;
    }

    public void setCarEntitySet(Set<CarEntity> carEntitySet) {
        this.carEntitySet = carEntitySet;
    }

}
