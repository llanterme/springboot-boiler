package za.co.clique.infrastructure.dao.rest.interfaces;

import za.co.clique.infrastructure.dao.rest.entities.DistanceMatrixEntity;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.util.HashMap;

/**
 * Created by Luke on 9/15/17.
 */
public interface DistanceRestDao {

    DistanceMatrixEntity getTechnicianDistanceMatrix(HashMap<String, String> coOrds) throws DaoException;
}
