package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.ApiEntity;

@Repository
@Transactional
public interface ApiInfoRepository extends CrudRepository<ApiEntity, Integer> {

}
