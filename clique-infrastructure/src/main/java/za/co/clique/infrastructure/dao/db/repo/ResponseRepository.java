package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.RequestEntity;
import za.co.clique.infrastructure.dao.db.entities.ResponseEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;

import java.util.List;

@Repository
@Transactional
public interface ResponseRepository extends JpaRepository<ResponseEntity, Integer> {

   @Query("SELECT p FROM ResponseEntity p WHERE LOWER(p.requestId) = LOWER(:requestId)")
   List<ResponseEntity> getTechnicianResponses(@Param("requestId") int requestId);

   @Modifying
   @Query("DELETE FROM ResponseEntity p WHERE p.requestId = :requestId AND p.status = 'Responded'")
   void deleteResponses(@Param("requestId") int requestId);

   @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM ResponseEntity c WHERE c.technicianId = :technicianId and c.requestId = :requestId")
   boolean technicianAlreadyResponded(@Param("technicianId") int technicianId, @Param("requestId") int requestId);

}
