package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.RequestEntity;
import za.co.clique.infrastructure.dao.db.entities.ResponseEntity;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;

import java.util.List;

@Repository
@Transactional
public interface RequestRepository extends JpaRepository<RequestEntity, Integer> {


    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM RequestEntity c WHERE c.userId = :userId")
    boolean requestAlreadyLogged(@Param("userId") int userId);


    @Query("SELECT p FROM RequestEntity p WHERE p.userId = :userId")
    RequestEntity getUserRequest(@Param("userId") int userId);

    @Query("SELECT p FROM RequestEntity p WHERE LOWER(p.status) = :status ")
    List<RequestEntity> getRequestsPerStatus(@Param("status") String status);


}
