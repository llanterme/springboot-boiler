package za.co.clique.infrastructure.dao.db.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.ApiEntity;
import za.co.clique.infrastructure.dao.db.entities.MakeEntity;

import java.util.List;


@Repository
@Transactional
public interface CarMakeRepository extends CrudRepository<MakeEntity, Integer> {

    @Query(value = "select make.make_id, make.name, model.make_id, model.name from make, model WHERE make.make_id = model.make_id", nativeQuery = true)
    List<Object[]> getMakeModels();

}

