package za.co.clique.infrastructure.dao.rest.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WeatherRequest {

    private String name;
    private String base;
    private  WindEntity wind;
    private Collection<WeatherEntity> weather;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WindEntity getWind() {
        return wind;
    }

    public void setWind(WindEntity wind) {
        this.wind = wind;
    }

    public Collection<WeatherEntity> getWeather() {
        return weather;
    }

    public void setWeather(Collection<WeatherEntity> weather) {
        this.weather = weather;
    }
}
