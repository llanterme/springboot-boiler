package za.co.clique.infrastructure.dao.rest.repo;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import za.co.clique.infrastructure.dao.rest.util.RestUtil;
import za.co.clique.infrastructure.dao.rest.entities.ErrorResponse;
import za.co.clique.infrastructure.dao.rest.entities.WeatherRequest;
import za.co.clique.infrastructure.dao.rest.handlers.RestTemplateResponseErrorHandler;


import java.io.IOException;

public class WeatherRepository {


    public void getWeather() {

        ObjectMapper objectMapper = new ObjectMapper();


        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        String url = "http://127.0.0.1:20001/weather.json";

        HttpEntity<?> httpEntity = new HttpEntity<>(requestHeaders);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        try {

        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
        String body = responseEntity.getBody();



            if (RestUtil.isError(responseEntity.getStatusCode())) {
                ErrorResponse error = objectMapper.readValue(body, ErrorResponse.class);
               // throw new HistoryLookupDaoException(error, "History not found for smart shopper number: " + smartShopperNumber);
            } else {
                WeatherRequest weatherRequest = objectMapper.readValue(body, WeatherRequest.class);
                //return meterHistoryResponseMapper(meterHistoryResponse, status);
                System.out.println(weatherRequest.getName());
                System.out.println(weatherRequest.getBase());
                System.out.println(weatherRequest.getWind().getDeg());

            }


        } catch (IOException e) {

        } catch (ResourceAccessException e) {

        }



    }

}


