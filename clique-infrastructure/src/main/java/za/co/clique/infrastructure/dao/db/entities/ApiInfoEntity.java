package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;

/**
 * Created by Luke on 4/26/17.
 */
@Entity
@Table(name = "api_info", schema = "clique", catalog = "")
public class ApiInfoEntity {
    private int apiInfoId;
    private String apiVersion;

    @Id
    @Column(name = "api_info_id")
    public int getApiInfoId() {
        return apiInfoId;
    }

    public void setApiInfoId(int apiInfoId) {
        this.apiInfoId = apiInfoId;
    }

    @Basic
    @Column(name = "api_version")
    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiInfoEntity that = (ApiInfoEntity) o;

        if (apiInfoId != that.apiInfoId) return false;
        if (apiVersion != null ? !apiVersion.equals(that.apiVersion) : that.apiVersion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = apiInfoId;
        result = 31 * result + (apiVersion != null ? apiVersion.hashCode() : 0);
        return result;
    }
}
