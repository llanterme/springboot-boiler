package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.ApiEntity;
import za.co.clique.infrastructure.dao.db.entities.DeviceEntity;
import za.co.clique.infrastructure.dao.db.entities.RequestEntity;

@Repository
@Transactional
public interface DeviceRepository extends CrudRepository<DeviceEntity, Integer> {

    @Query("SELECT p FROM DeviceEntity p WHERE p.id = :id")
    DeviceEntity getUserDevice(@Param("id") int id);

}
