package za.co.clique.infrastructure.dao.rest.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import za.co.clique.infrastructure.dao.rest.util.RestUtil;
import za.co.clique.infrastructure.dao.rest.entities.*;
import za.co.clique.infrastructure.dao.rest.handlers.RestTemplateResponseErrorHandler;
import za.co.clique.infrastructure.dao.rest.interfaces.CarRestDao;
import za.co.clique.infrastructure.exceptions.DaoException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@Repository
public class CarRestDaoImpl implements CarRestDao {

    public List<CarRestEntity> getCarMakes() throws DaoException {

        ObjectMapper objectMapper = new ObjectMapper();
        List<CarRestEntity> carMakesList = new ArrayList<>();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        String url = "http://www.surf4cars.co.za/Services/AjaxDataServices.asmx/GetLiveMakes";
        CarRequest carMakes = new CarRequest("","Make");

        HttpEntity<?> httpEntity = new HttpEntity<Object>(carMakes, requestHeaders);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        try {

            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
            String body = responseEntity.getBody();

            if (RestUtil.isError(responseEntity.getStatusCode())) {
                ErrorResponse error = objectMapper.readValue(body, ErrorResponse.class);
               throw new DaoException("Unable to get car makes. Error is " + error.getErrorMessage());
            } else {
                CarResponse carResponse = objectMapper.readValue(body, CarResponse.class);

            for (CarRestEntity m: carResponse.getD()) {
                    CarRestEntity aCar = new CarRestEntity();
                    aCar.setName(m.getName());
                    aCar.setValue(m.getValue());

                    carMakesList.add(aCar);
                }

                return carMakesList;

            }


        } catch (IOException e) {
            throw new DaoException();

        } catch (ResourceAccessException e) {
            throw new DaoException();
        }


    }

    public List<CarRestEntity> getCarModels(int webId) throws DaoException {

        ObjectMapper objectMapper = new ObjectMapper();
        List<CarRestEntity> carMakesList = new ArrayList<>();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        String url = "http://www.surf4cars.co.za/Services/AjaxDataServices.asmx/GetLiveModelRangeForMake";
        CarRequest carMakes = new CarRequest("Make:"+ webId,"ModelRange");

        HttpEntity<?> httpEntity = new HttpEntity<Object>(carMakes, requestHeaders);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

        try {

            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
            String body = responseEntity.getBody();

            if (RestUtil.isError(responseEntity.getStatusCode())) {
                ErrorResponse error = objectMapper.readValue(body, ErrorResponse.class);
                throw new DaoException("Unable to get car models. Error is " + error.getErrorMessage());
            } else {
                CarResponse carResponse = objectMapper.readValue(body, CarResponse.class);

                for (CarRestEntity m: carResponse.getD()) {
                    CarRestEntity aCar = new CarRestEntity();
                    aCar.setName(m.getName());
                    aCar.setValue(m.getValue());

                    carMakesList.add(aCar);
                }

                return carMakesList;

            }


        } catch (IOException e) {
            throw new DaoException();

        } catch (ResourceAccessException e) {
            throw new DaoException();
        }


    }


}
