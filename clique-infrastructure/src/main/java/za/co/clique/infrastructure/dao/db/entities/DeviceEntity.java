package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;

/**
 * Created by Luke on 10/26/17.
 */
@Entity
@Table(name = "device", schema = "clique", catalog = "")
public class DeviceEntity {
    private int deviceId;
    private int id;
    private String deviceToken;

    @Id
    @Column(name = "device_id")
    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "device_token")
    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceEntity that = (DeviceEntity) o;

        if (deviceId != that.deviceId) return false;
        if (id != that.id) return false;
        if (deviceToken != null ? !deviceToken.equals(that.deviceToken) : that.deviceToken != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deviceId;
        result = 31 * result + id;
        result = 31 * result + (deviceToken != null ? deviceToken.hashCode() : 0);
        return result;
    }
}
