package za.co.clique.infrastructure.dao.db.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.clique.infrastructure.dao.db.entities.UserEntity;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

  @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM UserEntity c WHERE c.emailAddress = :emailAddress")
    boolean userAlreadyRegistered(@Param("emailAddress") String emailAddress);

  @Query("SELECT p FROM UserEntity p WHERE LOWER(p.emailAddress) = LOWER(:emailAddress) AND LOWER(p.password) = LOWER(:password)")
  UserEntity authenticateUser(@Param("emailAddress") String emailAddress, @Param("password") String password);

}
