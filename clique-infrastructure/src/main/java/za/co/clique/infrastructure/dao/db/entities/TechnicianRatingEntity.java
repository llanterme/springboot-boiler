package za.co.clique.infrastructure.dao.db.entities;

import javax.persistence.*;

/**
 * Created by Luke on 9/28/17.
 */
@Entity
@Table(name = "technician_rating", schema = "clique", catalog = "")
public class TechnicianRatingEntity {
    private int technicianRatingId;
    private int userCount;
    private int ratingCount;
    private int technicianId;

    @Id
    @Column(name = "technician_rating_id")
    public int getTechnicianRatingId() {
        return technicianRatingId;
    }

    public void setTechnicianRatingId(int technicianRatingId) {
        this.technicianRatingId = technicianRatingId;
    }

    @Basic
    @Column(name = "user_count")
    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    @Basic
    @Column(name = "rating_count")
    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    @Basic
    @Column(name = "technician_id")
    public int getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(int technicianId) {
        this.technicianId = technicianId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TechnicianRatingEntity that = (TechnicianRatingEntity) o;

        if (technicianRatingId != that.technicianRatingId) return false;
        if (userCount != that.userCount) return false;
        if (ratingCount != that.ratingCount) return false;
        if (technicianId != that.technicianId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = technicianRatingId;
        result = 31 * result + userCount;
        result = 31 * result + ratingCount;
        result = 31 * result + technicianId;
        return result;
    }
}
