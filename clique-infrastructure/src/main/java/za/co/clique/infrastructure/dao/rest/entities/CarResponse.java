package za.co.clique.infrastructure.dao.rest.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarResponse {


    private Collection<CarRestEntity> d;

    public Collection<CarRestEntity> getD() {
        return d;
    }

    public void setD(Collection<CarRestEntity> d) {
        this.d = d;
    }
}
